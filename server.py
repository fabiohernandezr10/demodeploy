from flask import Flask, request,make_response,redirect,render_template
from flask.wrappers import Response
from Crud import Crud
import json 
app = Flask(__name__) #instancia de flask

"""
#forma de crear rutas 
@app.route('/')#ruta raiz 
#lo que se va a hacer cuando se acceda a esa ruta
#lo que se va a hacer se define a través de una funcion
def hello():  
    ip = request.remote_addr
    retorno = "su ip es: "+str(ip)  
    return retorno #esto sera mostrado en el navegador
"""
@app.route('/')
def index():
    cr = Crud("ec2-44-198-80-194.compute-1.amazonaws.com",
    "df8er5c6o7382b",
    "efefevyurmdjfx","eb3787e902fdda512a5af9d5a62d0f52f2ba29a054050eb59b93ad28d45618d3")
    buses = cr.leer_buses() #buses es una lista de tuplas, cada registro es una tupla
    lista_buses=[]
    for bus in buses:#bus es la tupla de cada registro 
        lista_buses.append({"id": bus[0],
     "modelo": bus[1], "capacidad":bus[2],
     "placa": bus[3], "fabricante":bus[4]})
    user_ip = request.remote_addr
    #context = {"ip":user_ip,"buses":lista_buses}
    return render_template('index.html',ip=user_ip,buses = lista_buses)
    

@app.route('/hello')
def hello():
    user_ip=request.cookies.get('user_ip')
    return "la ip del PC es: "+str(user_ip)

@app.route('/division')
def division():
    div = 10/5
    return "la division es: "+str(div)

@app.route('/multiplicar')
def multiplicar():
    mult = 10*5    
    return "la multiplicacion es-+ : "+str(mult)

@app.route('/ejercicio')
def ejercicio():
    numero = int(input("ingrese un numero: "))
    return "el cuadrado del numero-es: "+str(numero**2)

@app.route('/get_bus')
def get_bus():
    cr = Crud("ec2-44-198-80-194.compute-1.amazonaws.com",
    "df8er5c6o7382b",
    "efefevyurmdjfx","eb3787e902fdda512a5af9d5a62d0f52f2ba29a054050eb59b93ad28d45618d3")
    buses = cr.leer_buses() #buses es una lista de tuplas, cada registro es una tupla
    primer_bus = buses[0] #tomo el primer registro (la primera tupla)
    print(primer_bus)
    respuesta = json.dumps( {"id": primer_bus[0],
     "modelo": primer_bus[1], "capacidad":primer_bus[2],
     "placa": primer_bus[3], "fabricante":primer_bus[4]})
    cr.close()#cerrando la conexion 
    """
    primer_bus_string ="id: "+str(primer_bus[0])+" modelo: "+str(primer_bus[1]) +\
        "capacidad: "+str(primer_bus[2])+\
            " placa: "+primer_bus[3] + "fabricante: "+primer_bus[4]
    """
    return respuesta


@app.route("/get_buses")
def get_buses():
    cr = Crud("ec2-44-198-80-194.compute-1.amazonaws.com",
    "df8er5c6o7382b",
    "efefevyurmdjfx","eb3787e902fdda512a5af9d5a62d0f52f2ba29a054050eb59b93ad28d45618d3")
    buses = cr.leer_buses() #buses es una lista de tuplas, cada registro es una tupla
    lista_buses=[]
    for bus in buses:#bus es la tupla de cada registro 
        lista_buses.append({"id": bus[0],
     "modelo": bus[1], "capacidad":bus[2],
     "placa": bus[3], "fabricante":bus[4]})
    respuesta = json.dumps(lista_buses)
    return respuesta

@app.route("/registro",methods=["GET", "POST"])
def registro():
    
    ciudad = request.form.get("ciudad")
    print(f"la ciudad = {ciudad}")
    nombre = request.form.get("nombre")
    print(f"el nombre = {nombre}")
    return render_template('registro.html')

if __name__=="__main__":
    #punto de partida de ejecucion del programa    
    print("arrancando servidor...")
    app.run(debug=False,host="0.0.0.0")
